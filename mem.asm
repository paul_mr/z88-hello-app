        include "oz.def"
        include "vmstate.def"

        module mem

        xdef meminit

.meminit
        ;; allocate pool for memory
        ld      a, MM_S1                          ; will load it in segment 1 (zero is intutition)
        ld      b, 0                              ; 'always'
        CALL_OZ(OS_Mop)
        jp      C, done

        ;; memory pool id is now in IX, store it in the vmstate
        ld      (VMSTAT_BASE+vm_poolid), ix

        jp      done

.closemem
        CALL_OZ(OS_Mcl)                           ; close memory pool in IX

.done
        ret
