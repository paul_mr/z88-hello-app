DEBUG_BIN=../../ext/z88/z88apps/z88apps/intuition/debugS00.bin

OBJS=main.obj mem.obj

ASM_DEFS=-DMPM -DDEBUG

ASM=mpm

hello.epr: app.bin cardhdr.bin
	z88card -szc 32 hello.epr $(DEBUG_BIN) 3e0000 app.bin 3f0000 cardhdr.bin 3f3fc0

cardhdr.bin: cardhdr.asm
	$(ASM) $(ASM_DEFS) -b $<

app.bin: $(OBJS)
	$(ASM) -cz80 -o$@ $(ASM_DEFS) -b $(OBJS)

%.obj:	%.asm
	$(ASM) $(ASM_DEFS) -j $<

clean:	
	rm -vrf *.bin hello.6? *.map
