        module MAIN

        include "oz.def"
        include "vmstate.def"

        org     $C000                             ; should be loaded in segment 3

        xref    meminit

; *************************************************************************************
;
; The Application DOR:
;
.Main_Dor
        defb    0, 0, 0                           ; link to parent
        defb    0, 0, 0
        defb    0, 0, 0
        defb    $83                               ; DOR type - application ROM
        defb    DOREnd0-DORStart0                 ; total length of DOR
.DORStart0
        defb    '@'                               ; Key to info section
        defb    InfoEnd0-InfoStart0               ; length of info section
.InfoStart0
        defw    0                                 ; reserved...
        defb    'H'                               ; application key letter
        defb    0                                 ; I/O buffer / vars for FlashStore
        defw    0                                 ;
        defw    0                                 ; Unsafe workspace
        defw    vmstat_size                       ; Safe workspace
        defw    Hello_Entry                       ; Entry point of code in seg. 3
if DEBUG
        defb    $3E                               ; intution
else
        defb    0                                 ; bank binding to segment 0 (none)
endif
        defb    0                                 ; bank binding to segment 1 (none)
        defb    0                                 ; bank binding to segment 2 (none)
        defb    $3F                               ; bank binding to segment 3
        defb    AT_GO                             ; good application
        defb    0                                 ; no caps lock on activation
.InfoEnd0
        defb 'H'                                  ; Key to help section
        defb 12                                   ; total length of help
        defb    0,0,0                             ; topics
        defb    0,0,0                             ; commands
        defb    0,0,0                             ; help
        defb    0,0,0                             ; token base
        defb    'N'                               ; Key to name section
        defb    NameEnd0-NameStart0               ; length of name
.NameStart0
        defm    "HelloApp"
        defb    0
.NameEnd0
        defb    $FF
.DOREnd0

.hello_entry
if DEBUG
        call $2000                                ; intuition
endif

        call meminit

.done                                             ; error if A != 0
        CALL_OZ(OS_Bye)
